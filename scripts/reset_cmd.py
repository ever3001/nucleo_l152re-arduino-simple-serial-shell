#!/usr/bin/env python

import sys
import glob
import serial

def main():
    COMPort = sys.argv[1];
    command = sys.argv[2];
    serialPort = serial.Serial(port=COMPort, baudrate=115200, timeout=1)
    serialPort.write((command + "\r\n").encode())
    serialPort.close()

if __name__ == '__main__':
    if (len(sys.argv) != 3):
        print("Usage: reset_cmd.py <COM port> <command>")
        sys.exit(1)
    main()
