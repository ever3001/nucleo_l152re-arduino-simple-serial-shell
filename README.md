[![Build Status](https://gitlab.com/ever3001/nucleo_l152re-arduino-simple-serial-shell/badges/main/pipeline.svg)](https://gitlab.com/ever3001/nucleo_l152re-arduino-simple-serial-shell)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# Nucleo L152RE Arduino simple serial shell

Simple serial shell terminal with the Nucleo L152RE. I use the commands to control a relay module connected in the microcontroller.

## Contributing

If you would like to contribute, please see the [instructions](./CONTRIBUTING.md)

## License

This project is licensed under the MIT License - see the [LICENSE](./LICENSE) file for details.
