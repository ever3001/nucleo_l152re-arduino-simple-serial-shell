#include <Commands.h>

const String gg_message = " \n\r \
╭━━━━-╮ \n\r \
╰┃ ┣▇━▇ \n\r \
 ┃ ┃  ╰━▅╮ \n\r \
 ╰┳╯ ╰━━┳╯    FREE WIN \n\r \
  ╰╮ ┳━━╯     SUPER EASY \n\r \
 ▕▔▋ ╰╮╭━╮  NICE TUTORIAL \n\r \
╱▔╲▋╰━┻┻╮╲╱▔▔▔╲ \n\r \
▏  ▔▔▔▔▔▔▔  O O┃ \n\r \
╲╱▔╲▂▂▂▂╱▔╲▂▂▂╱ \n\r \
 ▏╳▕▇▇▕ ▏╳▕▇▇▕ \n\r \
 ╲▂╱╲▂╱ ╲▂╱╲▂╱ \n\r \
";

const String moo_message = " \n\r \
                 (__) \n\r \
                 (oo) \n\r \
           /------\\/ \n\r \
          / |    || \n\r \
         *  /\\---/\\ \n\r \
            ~~   ~~ \n\r \
...\"Have you mooed today?\"... \n\r \
";

int gg(int argc, char **argv)
{
  shell.println(gg_message);
  return EXIT_SUCCESS;
}

int moo(int argc, char **argv)
{
  shell.println(moo_message);
  return EXIT_SUCCESS;
}

int fortune(int argc, char **argv)
{
  // Get random index of the array
  int index = rand() % NB_FORTUNES;
  // Print random fortune
  shell.println(fortune_messages[index]);
  
  return EXIT_SUCCESS;
}