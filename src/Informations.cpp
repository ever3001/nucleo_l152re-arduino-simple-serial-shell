#include <Commands.h>

int show_ID(int argc, char **argv)
{
   shell.println(F("Running " __FILE__ ", Built " __DATE__));
   return EXIT_SUCCESS;
};

int echo(int argc, char **argv)
{
   auto lastArg = argc - 1;
   for (int i = 1; i < argc; i++)
   {

      shell.print(argv[i]);

      if (i < lastArg)
         shell.print(F(" "));
   }
   shell.println();

   return EXIT_SUCCESS;
}
