#include <Commands.h>
#include <stdint.h>
#include <string.h>

#define NB_RELAY_CHANNELS (4)
#define RELAY_RESET_DEFAULT_TIME_IN_MS (500)

const uint8_t gpioRelay[NB_RELAY_CHANNELS] = {
    13,
    12,
    11,
    10,
};

uint16_t default_channel = 0;

void relay_init_pins(void)
{
   for (uint16_t i = 0; i < NB_RELAY_CHANNELS; ++i)
   {
      shell.println(String("Init pin : ") + String(gpioRelay[i]));
      pinMode(gpioRelay[i], OUTPUT);
   }
   all_on(0, NULL);
}

int relay_print_default_channel(int argc, char **argv)
{
   shell.println(String("Default channel: ") + String(default_channel));

   return EXIT_SUCCESS;
}

int relay_set_default_channel(int argc, char **argv)
{
   uint16_t local_default_channel = atoi(argv[1]);

   if (local_default_channel > NB_RELAY_CHANNELS)
   {
      /* Print error */
      shell.println(F("ERROR please select a valid channel\r\n"));
   }
   else
   {
      default_channel = local_default_channel;
      relay_print_default_channel(0, NULL);
   }

   return EXIT_SUCCESS;
}

int all_off(int argc, char **argv)
{
   shell.println(F("Turning off all channels"));
   for (uint16_t i = 0; i < NB_RELAY_CHANNELS; ++i)
   {
      digitalWrite(gpioRelay[i], LOW);
   }
   return EXIT_SUCCESS;
}

int off(int argc, char **argv)
{
   /* if only "off" was send */
   if (argc == 1)
   {
      shell.println(F("Turning off default channel"));
      /* Turn off the default channel */
      digitalWrite(gpioRelay[default_channel], LOW);
   }
   /* if all command was send*/
   else if (strcmp(argv[1], "all") == 0)
   {
      all_off(0, NULL);
   }
   /* if a list of channels were send */
   else
   {
      for (uint16_t i = 1; i < argc; ++i)
      {
         uint16_t introduced_channel = atoi(argv[i]);
         if (introduced_channel < 0 || introduced_channel >= NB_RELAY_CHANNELS)
         {
            shell.println(String("ERROR The channel ") + String(introduced_channel) + String(" is not a valid channel"));
         }
         else
         {
            shell.println(String("Turning off channel ") + String(introduced_channel));
            digitalWrite(gpioRelay[introduced_channel], LOW);
         }
      }
   }

   return EXIT_SUCCESS;
}

int all_on(int argc, char **argv)
{
   shell.println(F("Turning on all channels"));
   for (uint16_t i = 0; i < NB_RELAY_CHANNELS; ++i)
   {
      digitalWrite(gpioRelay[i], HIGH);
   }
   return EXIT_SUCCESS;
}

int on(int argc, char **argv)
{
   /* if only "on" was send */
   if (argc == 1)
   {
      shell.println(F("Turning on default channel"));
      /* Turn on the default channel */
      digitalWrite(gpioRelay[default_channel], HIGH);
   }
   /* if all command was send*/
   else if (strcmp(argv[1], "all") == 0)
   {
      all_on(0, NULL);
   }
   /* if a list of channels were send */
   else
   {
      for (uint16_t i = 1; i < argc; ++i)
      {
         uint16_t introduced_channel = atoi(argv[i]);
         if (introduced_channel < 0 || introduced_channel >= NB_RELAY_CHANNELS)
         {
            shell.println(String("ERROR The channel ") + String(introduced_channel) + String(" is not a valid channel"));
         }
         else
         {
            shell.println(String("Turning on channel ") + String(introduced_channel));
            digitalWrite(gpioRelay[introduced_channel], HIGH);
         }
      }
   }

   return EXIT_SUCCESS;
}

int reset(int argc, char **argv)
{
   shell.println(F("Resetting channels"));

   off(argc, argv);
   delay(RELAY_RESET_DEFAULT_TIME_IN_MS);
   on(argc, argv);

   return EXIT_SUCCESS;
}
