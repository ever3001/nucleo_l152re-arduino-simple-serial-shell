#include <Arduino.h>

#include <Commands.h>

////////////////////////////////////////////////////////////////////////////////
void setup()
{
  relay_init_pins();

  Serial.begin(115200);
  while (!Serial)
  {
    // wait for serial port to connect. Needed for native USB port only
    // AND you want to block until there's a connection
    // otherwise the shell can quietly drop output.
  }

  shell.attach(Serial);
  /* Add all commands available */
  shell.addCommand(F("id?"), show_ID);
  shell.addCommand(F("echo"), echo);
  shell.addCommand(F("set_def_chan"), relay_set_default_channel);
  shell.addCommand(F("print_def_chan"), relay_print_default_channel);
  shell.addCommand(F("on"), on);
  shell.addCommand(F("off"), off);
  shell.addCommand(F("of"), off);
  shell.addCommand(F("reset"), reset);
  shell.addCommand(F("re"), reset);
  shell.addCommand(F("gg"), gg);
  shell.addCommand(F("moo"), moo);
  shell.addCommand(F("fortune"), fortune);

  delay(500);
  show_ID(0, NULL);
  Serial.println(F("Ready."));
}

////////////////////////////////////////////////////////////////////////////////
void loop()
{
  shell.executeIfInput();
}