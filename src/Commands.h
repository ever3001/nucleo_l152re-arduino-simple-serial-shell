#ifndef __COMMANDS_H
#define __COMMANDS_H

#include <Arduino.h>
#include <SimpleSerialShell.h>

#define NB_FORTUNES (363)

/**************************************** INFO COMMANDS */

/**
 * @brief Print the informations of the binary loaded to the board
 *
 * @example id?
 */
int show_ID(int argc, char **argv);

/**
 * @brief Simple version of Unix style 'echo'. Prints a string to attached stream (Serial, Serial1 etc.).
 *
 * @example echo "Hello world"
 *
 */
int echo(int argc, char **argv);

/**************************************** RELAY COMMANDS */
/**
 * @brief Init the pins for the relay module
 *
 */
void relay_init_pins(void);

/**
 * @brief Print the value of the default channel
 *
 * @example relay_print_default_chan
 *
 */
int relay_print_default_channel(int argc, char **argv);

/**
 * @brief Change the relay default channel.
 *
 * @example relay_set_def_chan 1
 *
 */
int relay_set_default_channel(int argc, char **argv);

/**
 * @brief Set all relay channels to off
 *
 * @example all_off
 *
 */
int all_off(int argc, char **argv);

/**
 * @brief Set a list of relay channels to off. If no list, default channel is going to be switched off
 *
 * @example off
 * @example off 1 2
 * @example off all
 *
 */
int off(int argc, char **argv);

/**
 * @brief Set all relay channels to on
 *
 * @example all_on
 *
 */
int all_on(int argc, char **argv);

/**
 * @brief Set a list of relay channels to on. If no list, default channel is going to be switched on
 *
 * @example on
 * @example on 1 2
 * @example on all
 *
 */
int on(int argc, char **argv);

/**
 * @brief Reset a list of relay channels. If no list, default channel is going to be reset
 *
 * @example reset
 * @example reset 1 2
 * @example reset all
 *
 */
int reset(int argc, char **argv);

/**************************************** FUNNY COMMANDS */
extern const String fortune_messages[NB_FORTUNES];

int gg(int argc, char **argv);
int moo(int argc, char **argv);
int fortune(int argc, char **argv);


#endif // __COMMANDS_H